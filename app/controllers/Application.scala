package controllers

import play.api._
import play.api.mvc._
import anorm._
import play.api.db._
import play.api.Play.current
import models.Task
import models.User
import play.api.data._
import play.api.data.Forms._

object Application extends Controller {
  
  val newtaskform = Form(
    tuple(
      "name" -> text,
      "description" -> text
    )
  )
  
  val deltasksform = Form(
    "deltasks" -> list(number)
  )
  
  val loginform = Form(
    tuple(
      "email" -> text,
      "password" -> text
    )
  )
  
  val updprogressform = Form(
    tuple(
      "progress" -> number,
      "taskid" -> number
    )
  )
  
  def index = Action { implicit request =>
      Ok(views.html.index())
  }
  
  def Authenticated(f: Request[AnyContent] => Result): Action[AnyContent] = {
    Action { implicit request =>
      if (request.session.get("connected").isDefined) {
        f(request)
      } else {
        Redirect(routes.Application.index).flashing("type" -> "alert-error", "alert" -> "Vous n'êtes pas identifié.")
      }
    }
  }
  
  def allTasks = Authenticated { implicit request =>
    val user = User.getByEmail(request.session.get("connected").get).get
    val tasks: List[Task] = Task.getAllFor(user.id)
    Ok(views.html.alltasks(tasks, newtaskform, updprogressform))
  }
  
  def newTask = Authenticated { implicit request =>
    val (name, description) = newtaskform.bindFromRequest()(request).get
    val user = User.getByEmail(request.session.get("connected").get).get
    Task.create(name, description, user.id) match {
      case true => Redirect(routes.Application.allTasks).flashing("type" -> "alert-success", "alert" -> "La tâche a été créée.")
      case false => Redirect(routes.Application.allTasks).flashing("type" -> "alert-error", "alert" -> "Erreur lors de la création de la tâche.")
    }
  }
  
  def delTasks = Authenticated { implicit request =>
    val tasks: List[Int] = deltasksform.bindFromRequest()(request).get
    val user = User.getByEmail(request.session.get("connected").get).get
    if (tasks.length > 0) {
      Task.deleteAll(tasks, user.id) match {
        case 0 => Redirect(routes.Application.allTasks).flashing("type" -> "alert-error", "alert" -> "0 tâche supprimée.")
        case 1 => Redirect(routes.Application.allTasks).flashing("type" -> "alert-success", "alert" -> "1 tâche supprimée.")
        case c => Redirect(routes.Application.allTasks).flashing("type" -> "alert-success", "alert" -> (c + " tâches supprimées."))
      }
    }
    else
      Redirect(routes.Application.allTasks).flashing("type" -> "alert-error", "alert" -> "Vous n'avez sélectionné aucune tâche.")
  }
  
  def updTaskProgress = Authenticated { implicit request =>
    val (progress, taskid) = updprogressform.bindFromRequest()(request).get
    val user = User.getByEmail(request.session.get("connected").get).get
    Task.updateProgress(taskid, user.id, progress)
    Redirect(routes.Application.allTasks).flashing("type" -> "alert-success", "alert" -> "Avancement mis à jour.")
  }
  
  def signin = Action { implicit request =>
    val (user, password) = loginform.bindFromRequest()(request).get
    User.authenticate(user, password) match {
      case Some(user) => Redirect(routes.Application.index).withSession("connected" -> user.email)
      case None => Redirect(routes.Application.index).flashing("type" -> "alert-error", "alert" -> "Adresse email ou mot de passe incorrect.")
    }
  }
  
  def signupForm = Action { implicit request =>
    Ok(views.html.signup(loginform))
  }
  
  def signup = Action { implicit request =>
    val (user, password) = loginform.bindFromRequest()(request).get
    User.create(user, password) match {
      case Some(user) => Redirect(routes.Application.index).flashing("type" -> "alert-success", "alert" -> "Félicitations ! Vous êtes maintenant inscrit !")
      case None => Redirect(routes.Application.index).flashing("type" -> "alert-error", "alert" -> "Cette adresse email est déjà utilisée.")
    }
  }
  
  def signoff = Authenticated { implicit request =>
    Redirect(routes.Application.index).withNewSession
  }
}