package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

case class Task(id: Long, name: String, description: String, owner: Long, progress: Int)

object Task {
  def getAllFor(ownerid: Long):List[Task] =  {
    DB.withConnection { implicit conn =>
      try {
        val tasks: List[(Long, String, String, Int)] = 
          SQL("SELECT id, name, description, progress FROM task WHERE owner = {owner} ORDER BY id ASC").on(
            'owner -> ownerid).as(long("id") ~ str("name") ~ str("description") ~ int("progress") map(flatten) *)

        var ret = List[Task]()
        tasks.map { t =>
          val task: Task = Task(t._1, t._2, t._3, ownerid, t._4)
          ret = task :: ret
        }

        ret
      } catch {
        case ex: Exception => { println(ex.getClass + "\n" + ex.getMessage); List[Task]() }
      }
    }
  }
  
  def create(name: String, description: String, ownerid: Long): Boolean = {
    DB.withConnection { implicit conn =>
      try {
        SQL("INSERT INTO task (name, description, owner, progress) VALUES ({name}, {description}, {owner}, 0)").on('name -> name, 'description -> description, 'owner -> ownerid).executeInsert()
        true
      } catch {
        case ex: Exception => { println(ex.getClass + "\n" + ex.getMessage); false }
      }
    }
  }
  
  def deleteAll(tasks: List[Int], ownerid: Long): Int = {
    DB.withConnection { implicit conn =>
      try {
        val ids = tasks.mkString(",");
        // I cannot use ids in the on function, maybe it gets escaped, but only the first row is deleted in case of multiple selection if I do
        SQL("DELETE FROM task WHERE id IN (" + ids + ") AND owner = {owner}").on('owner -> ownerid).executeUpdate()
      } catch {
        case ex: Exception => { println(ex.getClass + "\n" + ex.getMessage); 0 }
      }
    }
  }
  
  def updateProgress(id: Long, ownerid: Long, progress: Int) = {
    DB.withConnection { implicit conn =>
      try {
        SQL("UPDATE task SET progress = {progress} WHERE id = {id} AND owner = {owner}").on('progress -> progress, 'id -> id, 'owner -> ownerid).executeUpdate()
      } catch {
        case ex: Exception => { println(ex.getClass + "\n" + ex.getMessage); 0 }
      }
    }
  }
}
