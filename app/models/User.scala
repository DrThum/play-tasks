package models

import play.api.db._
import play.api.Play.current

import anorm._
import anorm.SqlParser._

import org.jasypt.util.password._

import com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException

case class User(id: Long, email: String)

object User {
  def create(email: String, password: String): Option[User] = {
    DB.withConnection { implicit conn =>
      try {
        val encryptor = new BasicPasswordEncryptor
        val hash = encryptor.encryptPassword(password)
        SQL("INSERT INTO user (email, password) VALUES ({email}, {password})").on('email -> email, 'password -> hash).executeInsert()
      } catch {
        case ex: MySQLIntegrityConstraintViolationException => None
      }
    } match {
      case Some(id) => Some(User(id, email))
      case None => None
    }
  }
  
  def authenticate(email: String, password: String): Option[User] = {
    DB.withConnection { implicit conn =>
      try {
        SQL("SELECT id, password FROM user WHERE email = {email}").on('email -> email).as(long("id") ~ str("password") map(flatten) *).head
      } catch {
        case ex: Exception => { println(ex.getMessage); None }
      }
    } match {
      case (id: Long, hash: String) => {
        val encryptor = new BasicPasswordEncryptor
        if (encryptor.checkPassword(password, hash))
          Some(User(id, email))
        else
          None
      }
      case None => None
    }
  }
  
  def getByEmail(email: String): Option[User] = {
    DB.withConnection { implicit conn =>
      try {
        SQL("SELECT id FROM user WHERE email = {email}").on('email -> email).as(scalar[Long].single)
      } catch {
        case ex: Exception => { println(ex.getMessage); None }
      }
    } match {
      case id: Long => Some(User(id, email))
      case None => None
    }
  }
}